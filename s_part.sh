#!/bin/bash
set -e

ok_mode=$1
mkdir /mnt/nfs
locale-gen
hwclock --systohc
sed -i 's/# deny = 3/deny = 0/g' /etc/security/faillock.conf
sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -z - --threads=0)/g' /etc/makepkg.conf
sed -i 's/COMPRESSGZ=(gzip -c -f -n)/COMPRESSGZ=(pigz -c -f -n)/g' /etc/makepkg.conf
sed -i 's/COMPRESSBZ2=(bzip2 -c -f)/COMPRESSBZ2=(pbzip2 -c -f)/g' /etc/makepkg.conf
sed -i 's/COMPRESSZST=(zstd -c -z -q -)/COMPRESSZST=(zstd -c -z -q - --threads=0)/g' /etc/makepkg.conf
sed -i 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j8"/g' /etc/makepkg.conf
echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf
echo '#!/bin/sh
CP=/bin/cp
exec $CP --reflink=auto "$@"' > /usr/local/bin/cp
sed -i 's/SigLevel    = Required DatabaseOptional/SigLevel    = Never/g' /etc/pacman.conf

#------------------

while pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com; [[ $? -ne 0 ]];
do
	echo "Trying again"
  sleep 5
done

while pacman-key --lsign-key FBA220DFC880C036; [[ $? -ne 0 ]];
do
	echo "Trying again"
  sleep 5
done

while pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst' --noconfirm; [[ $? -ne 0 ]];
do
	echo "Trying again"
  sleep 5
done

sed -i 's/#Color/Color\nILoveCandy/g' /etc/pacman.conf
sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 5/g' /etc/pacman.conf
echo '
[multilib]
Include = /etc/pacman.d/mirrorlist

[chaotic-aur]
Include = /etc/pacman.d/chaotic-mirrorlist

[okhsunrog-aur]
SigLevel = Never
Server = https://okhsunrog.ru/repos/$repo

[okhsunrog-repo]
SigLevel = Never
Server = https://okhsunrog.ru/repos/$repo

[archzfs]
SigLevel = Never
Server  = http://archzfs.com/$repo/x86_64
' >> /etc/pacman.conf

echo '#!/usr/bin/env bash

args=('--force' '--no-hostonly-cmdline')

while read -r line; do
	if [[ "$line" == 'usr/lib/modules/'+([^/])'/pkgbase' ]]; then
		read -r pkgbase < "/${line}"
		kver="${line#'usr/lib/modules/'}"
		kver="${kver%'/pkgbase'}"

		install -Dm0644 "/${line%'/pkgbase'}/vmlinuz" "/boot/vmlinuz-${pkgbase}"
		dracut "${args[@]}" "/boot/initramfs-${pkgbase}.img" --kver "$kver"
	fi
done
' > /usr/local/bin/dracut-install.sh

echo '#!/usr/bin/env bash

while read -r line; do
	if [[ "$line" == 'usr/lib/modules/'+([^/])'/pkgbase' ]]; then
		read -r pkgbase < "/${line}"
		rm -f "/boot/vmlinuz-${pkgbase}" "/boot/initramfs-${pkgbase}.img"
	fi
done
' > /usr/local/bin/dracut-remove.sh
chmod +x /usr/local/bin/*
mkdir /etc/pacman.d/hooks/
echo '
[Trigger]
Type = Path
Operation = Install
Operation = Upgrade
Target = usr/lib/modules/*/pkgbase

[Action]
Description = Updating linux initcpios (with dracut!)...
When = PostTransaction
Exec = /usr/local/bin/dracut-install.sh
Depends = dracut
NeedsTargets
' >> /etc/pacman.d/hooks/90-dracut-install.hook
echo '
[Trigger]
Type = Path
Operation = Remove
Target = usr/lib/modules/*/pkgbase

[Action]
Description = Removing linux initcpios...
When = PreTransaction
Exec = /usr/local/bin/dracut-remove.sh
NeedsTargets
' >> /etc/pacman.d/hooks/60-dracut-remove.hook


while pacman -Syyuu; [[ $? -ne 0 ]];
do
	echo "Trying again"
  sleep 5
done


while pacman -S dracut archlinux-keyring linux-lts linux-lts-headers zfs-dkms zfs-utils --noconfirm; [[ $? -ne 0 ]];
do
	echo "Trying again"
  sleep 5
done


#--------------------------------------------

echo "Installing additional software..."
while pacman -Syyuu --noconfirm; [[ $? -ne 0 ]];
do
	echo "Trying again"
  sleep 5
done

#arch repos
software0="ttf-jetbrains-mono-nerd blueman python-kikit python-pcbnewtransition thunar tumbler thunar-archive-plugin thunar-media-tags-plugin ark unarchiver noto-fonts noto-fonts-cjk noto-fonts-extra alsa-utils gtk-layer-shell zram-generator brightnessctl lximage-qt wireplumber intel-media-sdk openssl openssl-1.1 intel-media-driver intel-gpu-tools vulkan-intel libva-utils telegram-desktop vulkan-icd-loader vulkan-tools xorg-xrdb strawberry sof-firmware exfatprogs nfs-utils tmux screen dex ddcutil i2c-tools archiso bluez bluez-utils pacman-contrib smartmontools hdparm wayland-protocols hyphen-en hyphen gnome-keyring libgnome-keyring upower iotop f2fs-tools efitools efibootmgr dosfstools arch-install-scripts pv alsa-firmware pipewire pipewire-alsa pipewire-jack pipewire-pulse xdg-desktop-portal xdg-desktop-portal-wlr yt-dlp nm-connection-editor hunspell hunspell-en_us perl-file-mimeinfo sway swaybg swayidle mousepad cups cups-pdf usbutils go zsh i7z libappindicator-gtk3 lm_sensors stalonetray gst-libav gst-plugins-base gst-plugins-good gst-plugins-bad gst-plugins-ugly pigz pbzip2 android-udev libmad opus flac pcmanfm-qt fzf tree broot lxappearance qt5-wayland noto-fonts-emoji acpi systembus-notify ttf-dejavu otf-font-awesome xmlto pahole inetutils bc terminus-font reflector rsync cronie wf-recorder imagemagick tk python-pip zathura zathura-djvu zathura-pdf-mupdf udiskie udisks2 htop qt5ct qt6ct meson ninja scdoc playerctl libreoffice-fresh xorg-server-xwayland ffmpeg jdk-openjdk jdk8-openjdk mpv imv openssh wget ttf-opensans git neofetch pavucontrol grim slurp jq wl-clipboard neofetch cpio lhasa lzop p7zip unace unrar unzip zip earlyoom highlight mediainfo odt2txt perl-image-exiftool"
software0_okhsunrog=" android-tools cdrtools dvd+rw-tools xournalpp kicad kicad-library kicad-library-3d electrum keepassxc qutebrowser scrcpy docker docker-compose github-cli openscad skanlite libvncserver remmina wayvnc thunderbird ruby-bundler tor speedtest-cli virtualbox virtualbox-host-dkms virtualbox-guest-iso"
if [ "$ok_mode" = true ]; then
	software0+=$software0_okhsunrog
fi
while pacman -S $software0 --noconfirm --needed; [[ $? -ne 0 ]];
do
	echo "Trying again"
  sleep 5
done

#chaotic-aur
software1="network-manager-applet-nolibappindicator swaylock-effects-git clipman fcft foot i3status-rust libdispatch numix-icon-theme-git qt5-styleplugins qt6gtk2 yay"
software1_okhsunrog=" gimp-git librewolf ungoogled-chromium chromium-widevine virtualbox-ext-oracle"
if [ "$ok_mode" = true ]; then
	software1+=$software1_okhsunrog
fi
while pacman -S $software1 --noconfirm --needed; [[ $? -ne 0 ]];
do
	echo "Trying again"
  sleep 5
done

#my repos
pacman -Rnsdd xdg-utils --noconfirm
software2="python-emoji-fzf-okhsunrog wlr-sunclock-git atool2-git  gtk-theme-numix-solarized-git hunspell-ru-aot zrepl hyphen-ru lsix-git mako-no-blur-git mimeo ntfsprogs-ntfs3 swaykbdd sworkstyle throttled-git tiny-irc-client ttf-menlo-powerline-git ttf-ms-win11-okhsunrog wlogout-git xdg-utils-mimeo zsh-vi-mode-git way-displays"
software2_okhsunrog=" logseq-desktop-wayland-bin tabletsettings-okhsunrog-git python-grip openhantek6022-git ivpn electrum-ltc aria2-fast cava handbrake-full handbrake-full-cli puddletag qbittorrent-enhanced-qt5 ranger-sixel ruri ytop-okhsunrog-bin"
if [ "$ok_mode" = true ]; then
	software2+=$software2_okhsunrog
fi
while pacman -S $software2 --noconfirm --needed; [[ $? -ne 0 ]];
do
	echo "Trying again"
  sleep 5
done


#------------------------------------------

chsh -s $(which zsh)
read -p "Enter root password: " -s rpass
echo "$rpass
$rpass" | passwd
echo "Creating a new user..."
user_groups="wheel,video,uucp,i2c,lock,input"
if [ "$ok_mode" = true ]; then
	user_groups+=",vboxusers"
	uname=okhsunrog
else
	read -p "Enter user name: " uname
fi
useradd -mG $user_groups -s /usr/bin/zsh $uname
read -p "Enter $uname password: " -s upass
echo "$upass
$upass" | passwd $uname
sed -i 's/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/g' /etc/sudoers
chown -R $uname:$uname /vm
#echo "ALL ALL = (root) NOPASSWD: /usr/bin/hdparm" >> /etc/sudoers

#----------------------------------------

#mkdir /etc/docker
#echo '{ "features": { "buildkit": true } }' > etc/docker/daemon.json

ln -s /dev/null /etc/udev/rules.d/80-net-setup-link.rules

echo "#%PAM-1.0        
        
auth       required     pam_securetty.so        
auth       requisite    pam_nologin.so        
auth       include      system-local-login        
auth       optional     pam_gnome_keyring.so        
account    include      system-local-login        
session    include      system-local-login        
password   include      system-local-login        
session    optional     pam_gnome_keyring.so auto_start" >  /etc/pam.d/login
#curl -fsSL https://raw.githubusercontent.com/platformio/platformio-core/master/scripts/99-platformio-udev.rules | sudo tee /etc/udev/rules.d/99-platformio-udev.rules
#echo "i2c_dev" > /etc/modules-load.d/i2c.conf
echo 'ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"' > /etc/udev/rules.d/99-udisks2.rules
mkdir /etc/systemd/system/getty@tty1.service.d
echo "[Service]
ExecStart=
ExecStart=-/usr/bin/agetty --skip-login --login-options $uname --noclear %I $TERM" > /etc/systemd/system/getty@tty1.service.d/override.conf
cp /usr/lib/systemd/system/bluetooth.service /etc/systemd/system/
sed -i 's:ExecStart=/usr/lib/bluetooth/bluetoothd:ExecStart=/usr/lib/bluetooth/bluetoothd -E:g' /etc/systemd/system/bluetooth.service

sed -i 's/#export SAL_USE_VCLPLUGIN=gtk3/export SAL_USE_VCLPLUGIN=gtk3/g' /etc/profile.d/libreoffice-fresh.sh

#----------------------------------

systemctl enable NetworkManager  
systemctl enable earlyoom
systemctl enable zrepl
if [ "$ok_mode" = true ]; then
	systemctl enable docker
fi
#systemctl enable sshd
#systemctl enable nfs-server

#------------------------------------------

wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh
mv install.sh /home/$uname/
su - $uname -c 'sh install.sh --unattended'
rm /home/$uname/install.sh
git clone https://github.com/zsh-users/zsh-completions /home/$uname/.oh-my-zsh/custom/plugins/zsh-completions
git clone https://github.com/zsh-users/zsh-autosuggestions /home/$uname/.oh-my-zsh/custom/plugins/zsh-autosuggestions

#----------------------------------------

zpool set cachefile=/etc/zfs/zpool.cache zroot
systemctl enable zfs-import-cache.service
systemctl enable zfs-import.target
mkdir -p /etc/zfs/zfs-list.cache
systemctl enable zfs.target
systemctl enable zfs-zed.service
touch /etc/zfs/zfs-list.cache/zroot
zed -F &
sleep 1
zfs set canmount=off zroot/vm
sleep 1
zfs set canmount=on zroot/vm
zfs set org.zfsbootmenu:commandline="spl.spl_hostid=$(hostid) zbm.timeout=-1 zswap.enabled=0 bgrt_disable rw cryptomgr.notests noreplace-smp page_alloc.shuffle=1 tsc=reliable rcupdate.rcu_expedited=1 fbcon=font:TER16x32 quiet loglevel=3" zroot/ROOT
zfs set org.zfsbootmenu:keysource="zroot/ROOT/default" zroot

#--------------------
echo 'hostonly="no"
fscks="no"
install_items+=" /etc/zfs/zroot.key "
early_microcode="yes"
compress="zstd"' > /etc/dracut.conf.d/myflags.conf

echo "EDITOR=nvim" >> /etc/environment
echo "LOCALE=en_US.UTF-8
KEYMAP=ru
FONT=ter-i32b
CONSOLEMAP=
TIMEZONE=Europe/Moscow
HARDWARECLOCK=UTC
USECOLOR=yes" > /etc/vconsole.conf

#----------------------------------------

pacman -S linux-lts --noconfirm
mount -t efivarfs efivarfs /sys/firmware/efi/efivars
mkdir -p /boot/efi/EFI/ZBM
curl -o /boot/efi/EFI/ZBM/VMLINUZ.EFI -L https://get.zfsbootmenu.org/efi
cp /boot/efi/EFI/ZBM/VMLINUZ.EFI /boot/efi/EFI/ZBM/VMLINUZ-BACKUP.EFI

efibootmgr -c -d /dev/disk/by-partlabel/EFI \
  -L "ZFSBootMenu (Backup)" \
  -l \\EFI\\ZBM\\VMLINUZ-BACKUP.EFI | true

efibootmgr -c -d /dev/disk/by-partlabel/EFI \
  -L "ZFSBootMenu" \
  -l \\EFI\\ZBM\\VMLINUZ.EFI | true

#----------------------------------------

#configure pipewire

echo "stream.properties = {
    resample.quality      = 10
}
" > /etc/pipewire/client.conf.d/resampling.conf
echo "stream.properties = {
    resample.quality      = 10
}
" > /etc/pipewire/pipewire-pulse.conf.d/resampling.conf
echo "context.properties = {
    default.clock.allowed-rates = [ 44100 48000 88200 96000 192000 384000 ]
}
" > /etc/pipewire/pipewire.conf.d/resample_rates.conf

#-------------------------------------

#cp /net/*nmconnection /etc/NetworkManager/system-connections/
#cp /net/*conf /etc/wireguard/
#rm -rf /net

#------------------------------------------

sed -i "s?export GRIM_DEFAULT_DIR=/home/username/Pictures/screenshots?export GRIM_DEFAULT_DIR=/home/$uname/Pictures/screenshots?g" /.zprofile
sed -i "s?export ZSH=\"/home/username/.oh-my-zsh\"?export ZSH=\"/home/$uname/.oh-my-zsh\"?g" /.zshrc
sed -i "s?path+=('/home/username/.local/bin')?path+=('/home/""$uname""/.local/bin')?g" /.zshrc
sed -i "s?source /home/username/.config/broot/launcher/bash/br?source /home/$uname/.config/broot/launcher/bash/br?g" /.zshrc
sed -i "s?include \"/home/username/.gtkrc-2.0.mine\"?include \"/home/$uname/.gtkrc-2.0.mine\"?g" /.gtkrc-2.0

mkdir /etc/zrepl
mv /zrepl.yml /etc/zrepl/
mkdir -p /root/.config
mkdir -p /root/.local/share
mkdir -p /home/$uname/Pictures/screenshots
mv /Wallpapers /home/$uname/Pictures/Wallpapers
mv /.local /home/$uname/.local
mv /.config /home/$uname/.config
mv /after_install.sh /home/$uname/
mv /.gtkrc-2.0 /home/$uname/
mv /.zshrc /home/$uname/
ln -s /home/$uname/.zshrc /root/.zshrc
chmod 744 /root/.zshrc
mv /.zprofile /home/$uname/
mkdir -p /home/$uname/.local/share
cd /home/$uname/.local/share
aunpack /nvim.tar.zst
cd /
rm /nvim.tar.zst
mkdir -p /root/.local/share
mkdir -p /root/.config
ln -s /home/$uname/.local/share/nvim /root/.local/share/nvim
ln -s /home/$uname/.config/nvim /root/.config/nvim
chown -R $uname:$uname /home/$uname
chmod +x /home/$uname/.local/bin/*
mkdir /media

