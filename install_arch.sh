#!/bin/bash

pkill udiskie &> /dev/null
pkill udisksd &> /dev/null

rmmod pcspkr &> /dev/null
ok_mode=$1
bash logo.sh
if [ "$ok_mode" = true ]; then
             echo "Hi okhsunrog!"
             hsname="nova"
else
             read -p "Enter hostname (name of your PC): " hsname
fi
echo "Your hostname is $hsname"
ping -c 1 ya.ru &> /dev/null || { echo "No internet connection!"; exit; }
if [[ $EUID -ne 0 ]]; then    
    echo "You must be a root user to run this." 2>&1    
    exit 1    
fi

#--------------------------------------------------------

devices=$(lsblk -rdn -o NAME -e 7,11 | sed ':a;N;$!ba;s/\n/ /g')
devices_list=$(echo $devices)
devices_arr=($devices_list)
echo "Choose the target drive. Attention: everything on the target drive will be erased!"
echo "0) cancel"
i=1
for dv in ${devices_list}; do
  echo "$i) $dv"
  ((i++))
done
read -p "Enter option: " option
if [ "$option" = 0 ]; then
  echo "Bye!"
  exit 1
fi
[[ $option =~ ^[0-9]+$ ]] || { echo "Not a valid number, exiting..."; exit 1; }

if ((option >= 1 && option <= ${#devices_arr[@]})); then
  ((option--))
  _drive="/dev/${devices_arr[$option]}"
  echo "You have chosen $_drive"
else
  echo "The input is out of range! Exiting..."
  exit 1
fi

#--------------------------------------------------

echo "Installing needed packages"
sed -i 's/SigLevel    = Required DatabaseOptional/SigLevel    = Never/g' /etc/pacman.conf
pacman-key --init
pacman -Sy reflector --needed --noconfirm
echo "Installing ZFS"
bash zfs_init.sh -v

#-------------------------------------------------

mkdir -p /mnt/install
umount -R /mnt/install &> /dev/null
umount /mnt/install/boot/efi &> /dev/null
zfs umount -a &> /dev/null
zfs umount zroot/ROOT/default &> /dev/null
zpool export zroot &> /dev/null
umount ${_drive} &> /dev/null
umount ${_drive}p1 &> /dev/null
umount ${_drive}p2 &> /dev/null
umount ${_drive}p3 &> /dev/null
umount ${_drive}p4 &> /dev/null
umount ${_drive}1 &> /dev/null
umount ${_drive}2 &> /dev/null
umount ${_drive}3 &> /dev/null
umount ${_drive}4 &> /dev/null

#-------------------------------------------------

set -e
zgenhostid || true

#-------------------------------------------------

timedatectl set-ntp true
mkdir -p /etc/zfs/
read -p "Enter ZFS encryption password: " -s zfspass
echo "$zfspass" > /etc/zfs/zroot.key
chmod 000 /etc/zfs/zroot.key

sed -i 's/SigLevel    = Required DatabaseOptional/SigLevel    = Never/g' /etc/pacman.conf
sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 5/g' /etc/pacman.conf
reflector --verbose --sort rate --protocol https --country Russia --age 12 --save /etc/pacman.d/mirrorlist

#-------------------------

dd if=/dev/zero bs=512 count=34 status=progress oflag=sync of=${_drive}
dd if=/dev/zero of=${_drive} bs=512 count=34 seek=$((`blockdev --getsz ${_drive}` - 34))

cat <<EOF | gdisk ${_drive} 
o
y
n
1

+500M
ef00
c
EFI
n
2


bf00
c
2
rootpart
w
y
EOF
sleep 1
partprobe $_drive
sync
sleep 3
mkfs.fat -I -F32 -n EFI /dev/disk/by-partlabel/EFI
zpool create -f -o ashift=12         \
             -O acltype=posixacl       \
             -O relatime=on            \
             -O xattr=sa               \
             -o autotrim=on	\
             -O dnodesize=auto         \
             -O normalization=formD    \
             -O devices=off            \
             -O compression=lz4        \
             -O encryption=aes-256-gcm \
             -O keyformat=passphrase   \
             -O keylocation=file:///etc/zfs/zroot.key   \
	     -m none \
             zroot /dev/disk/by-partlabel/rootpart
sync
zfs create -o mountpoint=none zroot/data
zfs create -o mountpoint=none zroot/ROOT
zfs create -o mountpoint=/ -o canmount=noauto zroot/ROOT/default
zfs create -o mountpoint=/home zroot/data/home
zfs create -o mountpoint=/root zroot/data/root
zfs create -o mountpoint=/var -o canmount=off     zroot/var
zfs create                                        zroot/var/log
zfs create -o mountpoint=/var/lib -o canmount=off zroot/var/lib
zfs create                                        zroot/var/lib/libvirt
zfs create zroot/var/lib/AccountsService
zfs create zroot/var/lib/NetworkManager
zfs create                                        zroot/var/lib/docker
zfs create                                        zroot/var/cache
zfs create -o mountpoint=/vm                      zroot/vm
zpool set bootfs=zroot/ROOT/default zroot
zpool export zroot
zpool import -N -R /mnt/install zroot
zfs load-key zroot
zfs mount zroot/ROOT/default
zfs mount -a
zpool set cachefile=/etc/zfs/zpool.cache zroot
mkdir -p /mnt/install/etc/zfs/
cp /etc/zfs/zpool.cache /mnt/install/etc/zfs/zpool.cache
cp /etc/hostid /mnt/install/etc/
cp /etc/zfs/zroot.key /mnt/install/etc/zfs/

#------------------------------

mount -o X-mount.mkdir LABEL=EFI /mnt/install/boot/efi
#genfstab -L /mnt/install > /mnt/install/etc/fstab
echo "LABEL=EFI           	/boot/efi     	vfat      	rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro	0 2
" > /mnt/install/etc/fstab
while	pacstrap /mnt/install base base-devel openssl-1.1 linux-firmware intel-ucode man-db man-pages neovim networkmanager; [[ $? -ne 0 ]];
do
	echo "Trying again"
  sleep 5
done

#-----------------------------

echo $hsname > /mnt/install/etc/hostname
echo "127.0.0.1	localhost
::1		localhost
127.0.1.1	${hsname}.localdomain	${hsname}" > /mnt/install/etc/hosts
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /mnt/install/etc/locale.gen
echo "LANG=en_US.UTF-8" > /mnt/install/etc/locale.conf 

#-------------------------------

cp s_part.sh /mnt/install
cp after_install.sh /mnt/install
cp -r .config /mnt/install
cp -r .local /mnt/install
cp -r Wallpapers /mnt/install
cp .gtkrc-2.0 /mnt/install
cp .z1 /mnt/install/.zshrc
cp .z2 /mnt/install/.zprofile
cp zrepl.yml /mnt/install
cp nvim.tar.zst /mnt/install
#cp -r net /mnt/install

chmod +x /mnt/install/after_install.sh
chmod +x /mnt/install/s_part.sh
arch-chroot /mnt/install ./s_part.sh $ok_mode
rm /mnt/install/s_part.sh
sleep 1
echo "All done! Attemping unmount..."

sync
umount /mnt/install/boot/efi
umount /mnt/install/sys/firmware/efi/efivars | true
umount /mnt/install/sys | true
zfs umount -a
zfs umount zroot/ROOT/default
sleep 1
zfs mount zroot/ROOT/default
sed -Ei "s|/mnt/install/?|/|" /mnt/install/etc/zfs/zfs-list.cache/zroot
zfs umount zroot/ROOT/default
zpool export zroot

echo "You may now reboot."
